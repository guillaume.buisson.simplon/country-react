import {useState} from "react";

export default function CountryFormController() {
    const [inputValue, setInputValue] = useState('')

    const submit = () => {
        let re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(re.test(inputValue)) {
            alert(`mail envoyé a ${inputValue}`);
        } else {
            alert(`email incorrect`);
        }
    }

    const onChange = e => {
        setInputValue(e.target.value);
    }

    return(
        <div className="d-flex flex-column">
            <label htmlFor="email" className="my-2">Email : </label>
            <input onChange={onChange} name="email" className="my-2" type="email"/>
            <button className="my-2 btn btn-secondary" onClick={submit}>Envoyer</button>
        </div>
    )
}