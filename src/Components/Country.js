import {useEffect, useState} from "react";
import CountryFormController from "./CountryForm";

export default function GetAllCountry() {
    const [AllCountry, setAllCountry] = useState(null);
    const [CountryName, setCountryName] = useState(null);
    const [CountryNames, setCountryNames] = useState(null);

    useEffect( () => {
        fetch('https://restcountries.com/v3.1/all')
            .then(res => res.json())
            .then(json => setAllCountry(json))
            .catch(err=>console.log(err))
    }, [])

    const onClick = e => {
        let myArray = [];
        if (CountryName)
            myArray = CountryName;
        let checker = e.target;
        let countryName = e.target.dataset.country;
        if(myArray.length < 10) {
            if (myArray.indexOf(countryName) > -1) {
                myArray.splice(myArray.indexOf(countryName), 1)
            } else {
                myArray.push(countryName);
            }
            checker.innerText = checker.innerText === '✕' ? '✓' : '✕';
            setCountryName(myArray);

            const listCountryNames = myArray ? myArray.map((name) =>
                <div key={name} className="m-2 myTags">{name}</div>
            ) : null;

            setCountryNames(listCountryNames)
        }
    }

    const listCountry = AllCountry ? AllCountry.map((country) =>
        <div key={country.name.common} className="w-100 p-3 hover-country d-flex justify-content-between">
            <span className="mx-3">{country.flag}</span>
            <span className="country-text text-center">{country.name.common}</span>
            <span className="country-checked" data-country={country.name.common} onClick={onClick}>✕</span>
        </div>
    ) : null

    return(
        <div className="d-flex vh-100">
            <div id="country-select" className="p-5 shadow-lg">
                <p>Country :</p>
                <div className="w-100">
                    {listCountry}
                </div>
            </div>
            <div className="m-5 p-5 shadow-lg h-auto w-50">
                <div className="w-100 p-3 d-flex justify-content-center flex-wrap">
                    {CountryNames}
                </div>
                <CountryFormController />
            </div>
        </div>
    )
}