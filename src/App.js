import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import GetAllCountry from "./Components/Country";

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
          <div className="w-100 vh-100">
              <GetAllCountry />
          </div>
      </header>
    </div>
  );
}
